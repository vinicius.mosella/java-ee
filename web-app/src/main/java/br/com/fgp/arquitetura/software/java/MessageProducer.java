package br.com.fgp.arquitetura.software.java;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.json.JsonObject;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@Named
@RequestScoped
public class MessageProducer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private JMSContext ctx;

    @Resource(mappedName = "queue/PersistEventoQueue")
    private Queue evento;
    
    @Resource(mappedName = "queue/PersistUsuarioQueue")
    private Queue usuario;

    public void sendMessageEvento(JsonObject message) {
        ctx.createProducer().send(evento, message.toString());
    }

    public void sendMessageUsuario(JsonObject message) {
        ctx.createProducer().send(usuario, message.toString());
    }
    
}
