package br.com.fgp.arquitetura.software.java.usuario;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("usuario")
@RegisterRestClient(baseUri = "http://localhost:8080/server/rest")
public interface UsuarioRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Map<String, Object>> usuarios();

}
