package br.com.fgp.arquitetura.software.java.usuario;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import br.com.fgp.arquitetura.software.java.MessageProducer;

@Named
@RequestScoped
public class UsuarioController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private MessageProducer messageProducer;

    @Inject
    @RestClient
    private UsuarioRest usuarioRest;

    private String nome;
    private String sobrenome;
    private String dataNascimento;
    
    public void criarUsuario() {
        JsonObject json = Json.createObjectBuilder()
                .add("nome", nome)
                .add("sobrenome", sobrenome)
                .add("dataNascimento", dataNascimento)
                .build();
        messageProducer.sendMessageUsuario(json);

        addMessage("Usu�rio salvo com sucesso");
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<Map<String, Object>> getUsuarios() {
        return usuarioRest.usuarios();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

}
