package br.com.fgp.arquitetura.software.java.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.json.JsonObject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = "Usuario.getAll", query = "select u from Usuario u")
})
public class Usuario {

    private static final DateTimeFormatter INPUT_DATE_FORMATTER = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.ENGLISH);
    private static final DateTimeFormatter OUTPUT_DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH);

    @Id
    @GeneratedValue
    private long id;
    private String nome;
    private String sobrenome;
    private LocalDate dataNascimento;

    public Usuario() {
    }

    public Usuario(JsonObject jsonObject) {
        this(jsonObject.getString("nome"), jsonObject.getString("sobrenome"), jsonObject.getString("dataNascimento"));
    }

    public Usuario(String nome, String sobrenome, String dataNascimento) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.setDataNascimento(dataNascimento);
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getSobrenome() {
        return sobrenome;
    }
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
    public String getDataNascimento() {
        return dataNascimento != null ? dataNascimento.format(OUTPUT_DATE_FORMATTER) : null;
    }
    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento !=null ? LocalDate.parse(dataNascimento, INPUT_DATE_FORMATTER) : null;
    }

}
