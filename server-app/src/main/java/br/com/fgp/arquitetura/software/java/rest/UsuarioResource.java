package br.com.fgp.arquitetura.software.java.rest;


import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.fgp.arquitetura.software.java.model.Usuario;
import br.com.fgp.arquitetura.software.java.persistence.UsuarioPersist;

@Path("usuario")
public class UsuarioResource {

    @Inject
    private UsuarioPersist usuarioPersist;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Usuario> getUsuarios() {
        return usuarioPersist.listar();
    }

}
