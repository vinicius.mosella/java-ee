package br.com.fgp.arquitetura.software.java;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import br.com.fgp.arquitetura.software.java.rest.EventoResource;
import br.com.fgp.arquitetura.software.java.rest.UsuarioResource;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@ApplicationPath("rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(EventoResource.class);
        resources.add(UsuarioResource.class);
    }

}
