package br.com.fgp.arquitetura.software.java.queue;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import br.com.fgp.arquitetura.software.java.model.Usuario;
import br.com.fgp.arquitetura.software.java.persistence.UsuarioPersist;

@MessageDriven(mappedName = "queue/PersistUsuarioQueue",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        }
)
public class UsuarioMessageConsumer implements MessageListener {

    @Inject
    private UsuarioPersist usuarioPersist;

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;

            JsonReader jsonReader = Json.createReader(new StringReader(textMessage.getText()));
            JsonObject jsonObject = jsonReader.readObject();

            usuarioPersist.novoUsuario(new Usuario(jsonObject));
        } catch (JMSException ex) {
            Logger.getLogger(UsuarioMessageConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
