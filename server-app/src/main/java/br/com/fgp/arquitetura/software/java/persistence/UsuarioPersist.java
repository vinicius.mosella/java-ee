package br.com.fgp.arquitetura.software.java.persistence;

import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.fgp.arquitetura.software.java.model.Usuario;

@Named
public class UsuarioPersist {

    @PersistenceContext
    private EntityManager entityManager;

    public void novoUsuario(Usuario usuario) {
        entityManager.persist(usuario);
    }

    public List<Usuario> listar() {
        return entityManager.createNamedQuery("Usuario.getAll", Usuario.class).getResultList();
    }

}
